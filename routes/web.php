<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    //return view('welcome');
    return view('index');
});
*/
Route::get('/', 'MomentumController@index');

Route::get('/planet/{pageId}', 'MomentumController@planetPages');
Route::get('/people/{pageId}', 'MomentumController@peoplePages');
Route::get('/starship/{pageId}', 'MomentumController@starshipPages');
Route::post('/','MomentumController@store');
